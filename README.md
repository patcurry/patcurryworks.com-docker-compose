# patcurryworks.com-docker-compose

This will be the docker compose file I use to hold the front end, back end and database together.

The first thing to do is to write the django project and the static files to their own docker images. Then they will have to be stored on the gitlab docker repository, then this can be written and tested.

I assume that this can be written in a way that handles a new docker images as long as the have the same name or are the latest version. For example, the file and project should keep running if back-end:1.0 is updated to back-end:1.1. Presumably putting something like back-end:latest would handle that problem.
